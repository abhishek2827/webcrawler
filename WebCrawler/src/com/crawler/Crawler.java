package com.crawler;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import com.crawler.VO.*;
import com.crawler.dao.RestaurantsDAO;
public class Crawler {

	private AnchorVO anchor;
	private Document document;

	public Crawler (AnchorVO anchor) throws Exception
	{
		this.anchor = anchor;
	}
	public void loadDocumentFromWeb()
	{
		EntryVO entryVO= new EntryVO();
		entryVO.anchorVO = anchor ;
		try {
			document = Jsoup.connect(Common.urlGeneraor(anchor)).userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36").get();
			Elements content = document.getElementsByClass("js-fire-click-tracking-event");
			int num=0;
			for(Element e: content)
			{

				Elements links = e.getElementsByTag("a");
				for (Element link : links) {
					RestaurantsVO restaurantsVO = new RestaurantsVO();
					restaurantsVO.restaurantName = link.text(); 
					restaurantsVO.restaurantURL = link.attr("href");
					if(!(restaurantsVO.restaurantName.equals("Go to menu")||restaurantsVO.restaurantName.equals("Preorder now") || restaurantsVO.restaurantName.equals("")))
					{
						num ++;
						System.out.println(num+" = "+restaurantsVO.restaurantName);
						entryVO.restaurantsVO.add(restaurantsVO);
					}
					
				}
			}
			RestaurantsDAO.updateTable(entryVO);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
