package com.crawler.VO;

import java.sql.Timestamp;

import com.crawler.Common;

public class AnchorVO {

//	private Domain domain ;
	public String anchorURL ;
	public String city;
	public String location;
	public AnchorVO(String anchorURL, String city, String location) {
		super();
		this.anchorURL = anchorURL;
		this.city = city;
		this.location = location;
	}
	
	
}
