package com.crawler.controller;

import com.crawler.VO.*;
import com.crawler.Crawler;

public class Controller {

	public static void main(String args[]) throws Exception
	{
		String city, location, URL ;
		URL = "https://www.foodpanda.in/restaurants/city/";
		city = "pune";
		location = "yerwada";
		AnchorVO anchor = new AnchorVO(URL,city,location);
		Crawler crawler = new Crawler(anchor);
		crawler.loadDocumentFromWeb();
	}
}
