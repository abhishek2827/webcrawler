package com.crawler.dao;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;



public class dbConnection {

    private static Connection conn = null;

    public static Connection getConnection() {
    	
        if (conn != null)
            return conn;
        else {
        	try{
        		Properties prop = new Properties();
        		//prop.load(inputStream);
        		
        		InputStream inputStream = dbConnection.class.getClassLoader().getResourceAsStream("db.properties");
                prop.load(inputStream);
                String driver = prop.getProperty("driverName");
                String url = prop.getProperty("connURL");
                String user = prop.getProperty("userID");
                String password = prop.getProperty("password");
                Class.forName(driver);
                conn = DriverManager.getConnection(url, user, password);
           
        		
        		//Class.forName("com.mysql.jdbc.Driver");
    			//conn = DriverManager.getConnection("jdbc:mysql://localhost:3307/stock","root","password");
    			System.out.println("Connected to DB!");
			} catch(Exception e) {
				e.printStackTrace();
			}
            return conn;
        }

    }
}

